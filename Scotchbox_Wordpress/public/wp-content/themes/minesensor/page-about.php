<?php
/*
	Template Name: About Page
*/
?>

<?php get_header() ?>
<?php while( have_posts() ) : the_post() ?>
<div class="about">
<div class="head-banner" <?php echo header_banner_style(); ?>>
	<div class="overlay">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="banner-content">
						<h2><?php the_title() ?></h2>
						<div class="diver"></div>
						<p><?php echo $jarvis['minesensor_about'] ?></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><!--.head-banner-->

<div class="content">
    <div class="mission">
        <div class="container">
            <div class="row">
                <div class="col-xs-4">
                    <div class="mission-1"><img src="assets/dist/images/about/about-1.jpg"></div>
                </div>
                <div class="col-xs-4">
                    <div class="mission-2"><img src="assets/dist/images/about/about-2.jpg"></div>
                </div>
                <div class="col-xs-4">
                    <div class="mission-3"><img src="assets/dist/images/about/about-3.jpg"></div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="mission-content">
                        <?php the_content() ?>
                    </div>
                    <!--.mission-content-->
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php endwhile; ?>
<?php get_footer() ?>