<?php get_header();
global $jarvis;
    $banner_bg = $jarvis['minesensor_home_banner_background'];
    if (isset($banner_bg['url'])){
        $css = ' style="background-image: url('.$banner_bg['url'].')"';
    }
    $banner_image = $jarvis['minesensor_home_banner_image'];
    if (!isset($banner_image['url'])){
        $_banner_image = $jarvis['minesensor_home_banner_image']['url'];
    }
    else{
        $_banner_image = THEME_URL.'/assets/dist/images/home_figure-1.png';
    }
?>
<section id="top-banner"<?php echo $css; ?>>
    <div class="ms-overlay">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-sm-6 wow fadeInLeft">
                    <h2 class="ms-h2"><?php echo $jarvis['minesensor_home_banner_text']; ?></h2>
                    <a href="<?php echo get_page_link($jarvis['minesensor_product_page']) ?>"
                       class="ms-btn ms-btn-default ms-sweep-to-right"><?php echo $jarvis['minesensor_home_banner_button_text']; ?></a>
                </div>
                <div class="col-lg-7 col-sm-6 hidden-xs">
                    <img src="<?php echo $_banner_image; ?>" alt="figure-1" class="img-responsive wow slideInUp">
                </div>
            </div>
        </div>
    </div>
</section>
<section class="content-section">
    <div class="container text-center">
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <h2><?php echo $jarvis['minesensor_about_section_title']; ?></h2>

                <div class="line-1"></div>
                <p><?php echo $jarvis['minesensor_about'] ?></p>
            </div>
        </div>
    </div>
</section>
<section class="content-section bg-grey">
    <div class="container">
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1 text-center">
                <h2><?php echo $jarvis['minesensor_advantages_section_title']; ?></h2>

                <div class="line-1"></div>
                <div class="row">
                    <div class="box-item col-sm-4 wow fadeInLeft" data-wow-delay="1.5s">
                        <img class="img-responsive center-block"
                             src="<?php echo $jarvis['minesensor_advantages'][0]['image'] ?>"
                             alt="<?php echo strtolower($jarvis['minesensor_advantages'][0]['title']) ?>">
                        <h4><?php echo $jarvis['minesensor_advantages'][0]['title'] ?></h4>

                        <p><?php echo $jarvis['minesensor_advantages'][0]['description'] ?></p>
                    </div>
                    <div class="box-item col-sm-4 wow fadeInUp" data-wow-delay="1.2s">
                        <img class="img-responsive center-block"
                             src="<?php echo $jarvis['minesensor_advantages'][1]['image'] ?>"
                             alt="<?php echo strtolower($jarvis['minesensor_advantages'][1]['title']) ?>">
                        <h4><?php echo $jarvis['minesensor_advantages'][1]['title'] ?></h4>

                        <p><?php echo $jarvis['minesensor_advantages'][1]['description'] ?></p>
                    </div>
                    <div class="box-item col-sm-4 wow fadeInRight" data-wow-delay="1s">
                        <img class="img-responsive center-block"
                             src="<?php echo $jarvis['minesensor_advantages'][2]['image'] ?>"
                             alt="<?php echo strtolower($jarvis['minesensor_advantages'][2]['title']) ?>">
                        <h4><?php echo $jarvis['minesensor_advantages'][2]['title'] ?></h4>

                        <p><?php echo $jarvis['minesensor_advantages'][2]['description'] ?></p>
                    </div>
                    <div class="clearfix wow fadeInUp" data-wow-delay="2s">
                        <div class="col-xs-12">
                            <a href="<?php echo get_page_link($jarvis['minesensor_product_page']) ?>"
                               class="ms-btn ms-btn-info ms-sweep-to-right">
                                <?php echo $jarvis['minesensor_advantages_section_button_text']==''?__('Learn More About The Product','minesensor'):$jarvis['minesensor_advantages_section_button_text']; ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_template_part('sections/home/products'); ?>
<section id="testimonial">
    <div class="ms-overlay">
        <div class="container holder-section">
            <div class="text-center">
                <h2><?php echo $jarvis['minesensor_testimonial_section_title']; ?></h2>
                <div class="line-1"></div>
            </div>
            <div class="ms-slide">
                <?php
                $args = array('post_type' => 'testimonial');
                $testimonials = new WP_Query($args);

                if ($testimonials->have_posts()) : while ($testimonials->have_posts()) : $testimonials->the_post();
                    ?>
                    <div class="item">
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="row">
                                    <div class="col-sm-3 col-sm-offset-0 col-xs-4 col-xs-offset-4">
                                        <?php the_post_thumbnail('full', array('class' => 'img-responsive')) ?>
                                    </div>
                                    <div class="col-sm-9 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                                        <?php the_content() ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php endwhile;
                    wp_reset_postdata(); endif; ?>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>
