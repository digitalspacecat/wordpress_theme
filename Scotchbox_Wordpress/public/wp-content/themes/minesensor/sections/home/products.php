<section class="content-section wow fadeInUp">
    <div class="container">
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <div class="text-center">
                    <h2>
                        <?php
                        global $jarvis;
                        echo $jarvis['minesensor_product_section_title'] == '' ? 'OUR PRODUCTS' : $jarvis['minesensor_product_section_title'];
                        ?>
                    </h2>

                    <div class="line-1"></div>
                </div>
                <div class="line-space-70"></div>
                <?php
                $args = array('post_type' => 'product', 'posts_per_page' => $jarvis['minesensor_product_max']);
                $products = new WP_Query($args);
                $classes = array();
                $i = 1;
                if ($products->have_posts()) : while ($products->have_posts()) : $products->the_post();
                    $classes = ($i % 2 == 0) ? 'image-left' : 'image-right';
                    global $post;
                    ?>
                    <div class="home-product <?php echo $classes ?>">
                        <h3><?php the_title() ?></h3>

                        <div class="row">
                            <?php if ($i % 2 == 0) : ?>
                                <div class="col-sm-5 col-sm-offset-1 wow slideInLeft">
                                    <?php the_post_thumbnail('full', array('alt' => $post->post_name, 'class' => 'img-responsive')); ?>
                                    <?php
                                    if( class_exists('Dynamic_Featured_Image') ) {
                                        global $dynamic_featured_image;
                                        $featured_images = $dynamic_featured_image->get_featured_images(get_the_ID());

                                        $featured_2 = isset( $featured_images[0] ) ?
                                            '<div class="img"><img class="img-responsive center-block" src="'.$featured_images[0]['full'].'"></div>' :
                                            '';
                                    }
                                    ?>
                                </div>
                                <div class="col-sm-6 wow slideInRight">
                                    <?php
                                    if (get_field('product_homepage_text') == ''):
                                        the_content();
                                    else:
                                        the_field('product_homepage_text');
                                    endif;
                                    ?>
                                    <a href="<?php echo get_page_link($jarvis['minesensor_product_page']) ?>#<?php echo $post->post_name ?>"
                                       class="ms-btn ms-btn-info ms-sweep-to-right">LEARN MORE</a>
                                </div>
                            <?php else : ?>
                                <div class="col-sm-6 wow slideInLeft">
                                    <?php
                                    if (get_field('product_homepage_text') == ''):
                                        the_content();
                                    else:
                                        the_field('product_homepage_text');
                                    endif;
                                    ?>
                                    <a href="<?php echo get_page_link($jarvis['minesensor_product_page']) ?>#<?php echo $post->post_name ?>"
                                       class="ms-btn ms-btn-info ms-sweep-to-right">LEARN MORE</a>
                                </div>
                                <div class="col-sm-5 col-sm-offset-1 wow slideInRight">
                                    <?php the_post_thumbnail('full', array('alt' => $post->post_name, 'class' => 'img-responsive')); ?>
                                    <?php
                                    if( class_exists('Dynamic_Featured_Image') ) {
                                        global $dynamic_featured_image;
                                        $featured_images = $dynamic_featured_image->get_featured_images(get_the_ID());

                                        $featured_2 = isset( $featured_images[0] ) ?
                                            '<div class="img"><img class="img-responsive center-block" src="'.$featured_images[0]['full'].'"></div>' :
                                            '';
                                    }
                                    ?>
                                </div>
                            <?php endif; ?>
                            <div class="col-xs-12">
                                <div class="line-2 line-space-70"></div>
                            </div>
                        </div>
                    </div>
                    <?php $i++; endwhile;
                    wp_reset_query(); endif; ?>
            </div>
        </div>
    </div>
</section>