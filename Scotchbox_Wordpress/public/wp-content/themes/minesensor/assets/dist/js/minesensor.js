MS.site = (function ($) {
    var wow;
    return {
        testimonial: function (selector) {
            $(window).on('load', function () {
                $(selector).addClass('owl-carousel').owlCarousel({
                    items: 1,
                    nav: true,
                    navText: ['<i class="fa fa-angle-left fa-5x"></i>', '<i class="fa-5x fa fa-angle-right"></i>']
                });
            });
        },
        animate: function () {
            this.wow = new WOW().init();
        }
    }
})(jQuery);


jQuery(document).ready(function ($) {
    MS.site.testimonial('.ms-slide');
    MS.site.animate();

    $(window).on('resize', function () {
        var t = setInterval(function () {
            keep_testimonial();
            clearInterval(t);
        }, 300);
    });

    $(window).on('load', function () {
        var t = setInterval(function () {
            keep_testimonial();
            clearInterval(t);
        }, 500);
        keep_product();
    });

    function keep_testimonial() {
        var height = $('#testimonial .container').outerHeight();
        $('#testimonial').height(height);
    }

    function keep_product() {
        if ($(window).width() > 992) {
            $('.product-item .row').each(function(){
                var _this = $(this);
                var product_title = _this.find('.product-title');
                product_title.hide();
                var _h = _this.height();
                product_title.height(_h).show();

            });
        }
    }

    $('.menu-stack').on('click', function (e) {
        e.preventDefault();
        $('.main-nav ul').slideToggle();
    });


    $(window).resize(function (event) {
        if ($(window).width() > 480) {
            $('.main-nav ul').css('display', 'block');
        } else {
            $('.main-nav ul').css('display', 'none');
        }

        keep_product();
    });
});