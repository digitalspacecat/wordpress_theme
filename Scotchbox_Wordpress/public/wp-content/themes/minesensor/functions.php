<?php

class MineSensor
{

    protected $jarvis, $themeUrl;

    function __construct()
    {

    }

    function launch()
    {
        $this->loadDependencies();

        if (!is_admin()) {
            add_action('wp_enqueue_scripts', array($this, 'loadFrontEndAssets'));
        }
        add_action('wp_head',array($this,'favicons'));

        $this->jarvis = new VtmJarvis();

        //add_action('wp_loaded', array($this, 'htmlStartFilter'));
    }

    function htmlStartFilter()
    {
        ob_start(array($this, 'htmlEndFilter'));
    }

    function htmlEndFilter($html)
    {
        $html = str_replace('assets/dist/images', VtmJarvis::themeUrl('/assets/dist/images'), $html);
        return $html;
    }

    function favicons(){
        $site_url = get_bloginfo('url');
        ?>
        <link rel="apple-touch-icon" sizes="57x57" href="<?php echo $site_url; ?>/apple-touch-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?php echo $site_url; ?>/apple-touch-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo $site_url; ?>/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo $site_url; ?>/apple-touch-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo $site_url; ?>/apple-touch-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo $site_url; ?>/apple-touch-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?php echo $site_url; ?>/apple-touch-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo $site_url; ?>/apple-touch-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $site_url; ?>/apple-touch-icon-180x180.png">
        <link rel="icon" type="image/png" href="<?php echo $site_url; ?>/favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="<?php echo $site_url; ?>/favicon-194x194.png" sizes="194x194">
        <link rel="icon" type="image/png" href="<?php echo $site_url; ?>/favicon-96x96.png" sizes="96x96">
        <link rel="icon" type="image/png" href="<?php echo $site_url; ?>/android-chrome-192x192.png" sizes="192x192">
        <link rel="icon" type="image/png" href="<?php echo $site_url; ?>/favicon-16x16.png" sizes="16x16">
        <link rel="manifest" href="<?php echo $site_url; ?>/manifest.json">
        <meta name="msapplication-TileColor" content="#ffc40d">
        <meta name="msapplication-TileImage" content="<?php echo $site_url; ?>/mstile-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <?php
    }

    function loadFrontEndAssets()
    {
        wp_register_script('minesensor-lib', VtmJarvis::themeUrl('/assets/dist/js/minesensor.lib.js'), array('jquery'));
        wp_localize_script('minesensor-lib', 'MS', array());
        wp_enqueue_style('lib', VtmJarvis::themeUrl('/assets/dist/css/minesensor.lib.css'));
        wp_enqueue_style('site', VtmJarvis::themeUrl('/assets/dist/css/minesensor.css'));
        wp_enqueue_script('minesensor-lib');
        wp_enqueue_script('site', VtmJarvis::themeUrl('/assets/dist/js/minesensor.js'));
    }

    function loadDependencies()
    {

        add_theme_support( 'post-thumbnails', array( 'post', 'product', 'partner', 'testimonial' ) );
        add_theme_support( 'title-tag' );

        register_nav_menu('top-menu', 'Top Main Menu');
    }

}


include __DIR__ . '/functions/product_post_type.php';
include __DIR__ . '/functions/partner_post_type.php';
include __DIR__ . '/functions/testimonial_post_type.php';
add_action('init', 'launch_theme');

function launch_theme()
{
    $minesensor = new MineSensor();
    $minesensor->launch();
}

function header_banner_style($post_id = null){
    if ($post_id == null){
        global $post;
        $post_id = $post->ID;
    }
    $banner = get_field('banner_image',$post_id);
    if ($banner != ''){
        return "style='background-image: url(".$banner.")'";
    }
}