<?php
/*
	Template Name: Contact Page
*/

get_header();
global $jarvis;
?>

<div class="contact">

<div class="head-banner" <?php echo header_banner_style(); ?>>
	<div class="overlay">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="banner-content">
						<h2><?php the_field('page_custom_title'); ?></h2>
						<div class="diver"></div>
						<div class="container-fluid">
							<div class="row">
								<div class="col-xs-6">
									<img src="<?php echo THEME_URL; ?>/assets/dist/images/contact/phone-1.png">
									<p class="phone"><?php echo $jarvis['minesensor_contact_phone'] ?></p>
									<p class="desc"><?php echo $jarvis['minesensor_contact_phone_label'] ?></p>
								</div>

								<div class="col-xs-6">
									<img src="<?php echo THEME_URL; ?>/assets/dist/images/contact/email.png">
									<p class="email"><?php echo $jarvis['minesensor_contact_email'] ?></p>
									<p class="desc"><?php echo $jarvis['minesensor_contact_email_label'] ?></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><!--.head-banner-->

<div class="content">
	<div class="offices">
		<div class="container">
			<h3>our offices</h3>
			<div class="diver"></div>

			<div class="row">
			<?php
				if( have_rows('offices_info') ):

				// loop through the rows of data
				while ( have_rows('offices_info') ) : the_row(); ?>

				<div class="col-md-6">
					<div class="office-info">
						<h4><?php the_sub_field('name') ?></h4>
						<ul class="phone">


							<?php if ( get_sub_field('home_phone') ) {
								echo '<li><img src="'.THEME_URL.'/assets/dist/images/contact/phone-3.png"> '.get_sub_field('home_phone').'</li>';
							} ?>

							<?php if ( get_sub_field('mobile_phone') ) {
								echo '<li><img src="'.THEME_URL.'/assets/dist/images/contact/phone-2.png"> '.get_sub_field('mobile_phone').'</li>';
							} ?>

							<?php if ( get_sub_field('fax') ) {
								echo '<li>FAX: '.get_sub_field('fax').'</li>';
							} ?>
						</ul>
						<p><?php the_sub_field('address') ?></p>
						<div class="map"><?php the_sub_field('google_map_embed_code') ?></div>
					</div><!--.office-info-->
				</div>

				<?php endwhile; endif; ?>
			</div>
		</div>
	</div><!--.offices-->

	<div class="contact-form-wrap">
		<div class="overlay">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<div class="contact-form">
							<h3>send us a message</h3>
							<div class="diver"></div>
							<?php echo do_shortcode('[contact-form-7 id="48" title="Contact form"]'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div><!--.contact-form-wrap-->
</div><!--.content-->

</div>

<?php get_footer(); ?>