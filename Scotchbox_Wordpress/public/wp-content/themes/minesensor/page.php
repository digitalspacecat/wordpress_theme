<?php
/*
	Template Name: About Page
*/
?>

<?php get_header() ?>
<?php while( have_posts() ) : the_post() ?>
    <div class="about">
        <div class="head-banner" <?php echo header_banner_style(); ?>>
            <div class="overlay">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="banner-content">
                                <h2><?php the_field('page_custom_title'); ?></h2>
                                <div class="diver"></div>
                                <p><?php the_field('page_subtitle'); ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>N
        </div><!--.head-banner-->

        <div class="content">
            <div class="mission">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="mission-content">
                                <?php the_content() ?>
                            </div>
                            <!--.mission-content-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endwhile; ?>
<?php get_footer() ?>