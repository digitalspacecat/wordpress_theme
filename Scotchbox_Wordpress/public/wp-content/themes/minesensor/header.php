<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head(); ?>
</head>
<body <?php body_class() ?>>
<?php global $jarvis; ?>
<div class="site-header">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3">
                <div class="logo">
                    <a href="<?php bloginfo('home') ?>"><img src="<?php echo THEME_URL; ?>/assets/dist/images/logo-blue.png"></a>
                </div>
            </div>

            <div class="col-md-9 col-sm-9">
                <div class="main-nav">

                    <p class="menu-stack">
                        <a href="#"><i class="fa fa-bars"></i> Menu</a>
                    </p>

                    <?php
                        $args = array(
                            'theme_location' => 'top-menu',
                            'menu' => '',
                            'container' => '',
                            'menu_class' => 'menu',
                        );
                        wp_nav_menu( $args ); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!--.site-header-->