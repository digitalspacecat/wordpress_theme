<?php
/*
	Template Name: Product Page
*/
?>
<?php get_header() ?>
<?php while( have_posts() ) : the_post() ?>
<div class="product">
<div class="head-banner" <?php echo header_banner_style(); ?>>
	<div class="overlay">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="banner-content">
						<h2><?php the_title() ?></h2>
						<div class="diver"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><!--.head-banner-->

<div class="content">
<?php
	$args = array( 'post_type' => 'product' );
	$products = new WP_Query( $args ); $i = 1;
	if ( $products->have_posts() ) : while ( $products->have_posts() ) : $products->the_post();
	$i = ($i > 3) ? 1 : $i++;
	global $post;
?>
	<div class="product-item" id="<?php echo $post->post_name ?>">
		<div class="row">
			<div class="col-md-4 col-xs-12">
				<div class="product-title product-<?php echo $i; ?>">
					<div class="inner">
						<h3><?php the_title() ?></h3>
					</div>
				</div>
			</div>
			<?php
				// Frist Featured Image
				$post_thumbnail_id = get_post_thumbnail_id();
				$post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );
				$featured_1 = '<div class="img"><img class="img-responsive center-block" src="'.$post_thumbnail_url.'"></div>';

				// Secondary Featured Image
				if( class_exists('Dynamic_Featured_Image') ) {
			       global $dynamic_featured_image;
			       $featured_images = $dynamic_featured_image->get_featured_images(get_the_ID());

			       $featured_2 = isset( $featured_images[0] ) ?
			       				'<div class="img"><img class="img-responsive center-block" src="'.$featured_images[0]['full'].'"></div>' :
			       				'';
			   }
			?>
			<div class="col-md-4 col-xs-6"><?php echo $featured_1 ?></div>
			<div class="col-md-4 col-xs-6"><?php echo $featured_2 ?></div>
		</div>

		<div class="container">
			<div class="product-description">
				<h3><?php the_title() ?></h3>
                <div><?php the_content() ?></div>
			</div>
		</div>
	</div>
<?php endwhile; wp_reset_query(); endif;?>
</div><!--.content-->
</div>
<?php endwhile; ?>
<?php get_footer() ?>