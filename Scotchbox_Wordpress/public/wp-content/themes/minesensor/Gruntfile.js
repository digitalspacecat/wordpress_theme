module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
            },
            lib: {
                files: {
                    'assets/dist/js/<%= pkg.name %>.lib.js': [
                        'bower_components/OwlCarousel2/dist/owl.carousel.js',
                        'bower_components/WOW/dist/wow.js',
                        'bower_components/retinajs/dist/retina.js'
                    ]
                }
            }
        },
        cssmin: {
            lib: {
                files: {
                    'assets/dist/css/<%= pkg.name %>.lib.css':
                        [
                            'assets/src/css/**/*.css',
                            'bower_components/OwlCarousel2/dist/assets/owl.carousel.min.css',
                            'bower_components/fontawesome/css/font-awesome.css',
                            'bower_components/animate.css/animate.css'
                        ]
                }
            }
        },
        sass: {
            lib: {
                files: {
                    'assets/src/css/boostrap.css':'sass/bootstrap.scss',
                    'assets/src/css/hover.css':'bower_components/Hover/scss/hover.scss'
                }
            },
            site: {
                files: {
                    'assets/dist/css/<%= pkg.name %>.css':'sass/main.scss'
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    grunt.registerTask('site-css', ['sass:site']);
    grunt.registerTask('lib-css', ['sass:lib','cssmin:lib']);
    grunt.registerTask('lib-js', ['uglify:lib']);

};