<?php
function about_section_002() {
	return array(
	    'title'=>__('About section','minesensor'),
	    'desc'=>__('Manage about section','minesensor'),
        'icon' => 'el el-th',
		'fields'=>array(
            array(
                'id'       => 'minesensor_about_section_title',
                'type'     => 'text',
                'title'    => __('Section title', 'minesensor'),
            ),
            array(
                'id'       => 'minesensor_about',
                'type'     => 'textarea',
                'title'    => 'About',
                'subtitle' => 'About our company',
                'validate' => 'html_custom',
                'allowed_html' => array(
                    'a' => array(
                        'href' => array(),
                        'title' => array()
                    ),
                    'br' => array(),
                    'em' => array(),
                    'strong' => array()
                )
            )
        )
    );
}