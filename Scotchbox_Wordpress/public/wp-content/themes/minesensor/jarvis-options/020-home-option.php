<?php
function home_option_020() {

	return array(
	    'title'=>__('Home General Option','minesensor'),
	    'desc'=>__('Manage homepage','minesensor'),
        'icon' => 'el el-th',
		'fields'=>array(
			array(
			    'id'       => 'minesensor_product_page',
			    'type'     => 'select',
			    'title'    => __('Select Product Page', 'minesensor'),
			    'subtitle' => __('Select product page to use in homepage', 'minesensor'),
			    'data'  => 'pages',
			),

            array(
                'id'        => 'minesensor_product_max',
                'type'      => 'text',
                'title'     => __('Products Max', 'redux-framework-demo'),
                'subtitle'  => __('Maximum products will show in homepage', 'redux-framework-demo'),
                "default"   => 4
            )
        )
    );
}
?>