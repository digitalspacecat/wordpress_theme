<?php
function advantages_section_002() {
	return array(
	    'title'=>__('Advantages section','minesensor'),
        'icon' => 'el el-th',
		'fields'=>array(
            array(
                'id'       => 'minesensor_advantages_section_title',
                'type'     => 'text',
                'title'    => __('Section title', 'minesensor'),
            ),
            array(
                'id'       => 'minesensor_advantages_section_button_text',
                'type'     => 'text',
                'title'    => __('Button Text', 'minesensor'),
            ),
            array(
                'id'          => 'minesensor_advantages',
                'type'        => 'slides',
                'title'       => __('Minesensor Advantages', 'minesensor'),
                'subtitle'    => __('The advantages of minesensor', 'minesensor'),
                'placeholder' => array(
                    'title'           => __('This is a title', 'minesensor'),
                    'description'     => __('Description Here', 'minesensor'),
                    'url'             => __('', 'minesensor'),
                ),
            )
        )
    );
}
?>