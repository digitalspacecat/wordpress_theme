<?php
function a_userguide_001()
{

    return array(
        'title' => __('User guide', 'minesensor'),
        'icon' => 'el el-book',
        'fields' => array(array(
            'id'       => 'opt-raw',
            'type'     => 'raw',
            'title'    => __('FYI', 'redux-framework-demo'),
            'content'  => file_get_contents(dirname(__FILE__) . '/userguide.html')
        ))
    );
}