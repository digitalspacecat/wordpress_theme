<?php
function home_banner_001()
{

    return array(
        'title' => __('Home Banner Option', 'minesensor'),
        'icon' => 'el el-th',
        'fields' => array(
            array(
                'id' => 'minesensor_home_banner_text',
                'type' => 'text',
                'title' => __('Home banner text', 'redux-framework-demo')
            ),
            array(
                'id' => 'minesensor_home_banner_button_text',
                'type' => 'text',
                'title' => __('Home banner button text', 'redux-framework-demo'),
                'default'=>'Learn More About Our Product'
            ),
            array(
                'id'       => 'minesensor_home_banner_image',
                'type'     => 'media',
                'url'      => true,
                'title'    => __('Upload banner image', 'redux-framework-demo')
            ),
            array(
                'id'       => 'minesensor_home_banner_background',
                'type'     => 'media',
                'url'      => true,
                'title'    => __('Upload banner background image', 'redux-framework-demo')
            )
        )
    );
}