<?php
function testimonial_section_004() {
	return array(
	    'title'=>__('Testimonial section','minesensor'),
        'icon' => 'el el-th',
		'fields'=>array(
            array(
                'id'       => 'minesensor_testimonial_section_title',
                'type'     => 'text',
                'title'    => __('Section title', 'minesensor'),
            )
        )
    );
}
?>