<?php
function product_section_002() {
	return array(
	    'title'=>__('Products section','minesensor'),
	    'desc'=>__('Manage product section','minesensor'),
        'icon' => 'el el-th',
		'fields'=>array(
            array(
                'id'       => 'minesensor_product_section_title',
                'type'     => 'text',
                'title'    => __('Section title', 'minesensor'),
            )
        )
    );
}
?>