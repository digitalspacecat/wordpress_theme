<?php
function contact_info_010() {

	return array(
	    'title'=>__('Contact Info','minesensor'),
	    'desc'=>__('Make sure you understand what you are doing','minesensor'),
        'icon' => 'el el-th',
		'fields'=>array(
			array(
			    'id'       => 'minesensor_contact_phone_label',
			    'type'     => 'text',
			    'title'    => __('Contact Phone Label', 'minesensor'),
			),

			array(
			    'id'       => 'minesensor_contact_phone',
			    'type'     => 'text',
			    'title'    => __('Enter Contact Phone', 'minesensor'),
			),

			array(
			    'id'       => 'minesensor_contact_email_label',
			    'type'     => 'text',
			    'title'    => __('Contact Email Label', 'minesensor'),
			),

			array(
			    'id'       => 'minesensor_contact_email',
			    'type'     => 'text',
			    'title'    => __('Enter Contact Email', 'minesensor'),
			    'validate' => 'email'
			),
		),
	);
}
?>