<?php
// Register Custom Post Type
function product_post_type() {

	$labels = array(
		'name'                => _x( 'Products', 'Post Type General Name', 'minesensor' ),
		'singular_name'       => _x( 'Product', 'Post Type Singular Name', 'minesensor' ),
		'menu_name'           => __( 'Products', 'minesensor' ),
		'name_admin_bar'      => __( 'Products', 'minesensor' ),
		'parent_item_colon'   => __( 'Parent Item:', 'minesensor' ),
		'all_items'           => __( 'All Products', 'minesensor' ),
		'add_new_item'        => __( 'Add New Product', 'minesensor' ),
		'add_new'             => __( 'Add New', 'minesensor' ),
		'new_item'            => __( 'New Product', 'minesensor' ),
		'edit_item'           => __( 'Edit Product', 'minesensor' ),
		'update_item'         => __( 'Update Product', 'minesensor' ),
		'view_item'           => __( 'View Item', 'minesensor' ),
		'search_items'        => __( 'Search Item', 'minesensor' ),
		'not_found'           => __( 'Not found', 'minesensor' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'minesensor' ),
	);
	$args = array(
		'label'               => __( 'Product', 'minesensor' ),
		'description'         => __( 'Product Minesensor', 'minesensor' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-screenoptions',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'product', $args );

}
add_action( 'init', 'product_post_type');