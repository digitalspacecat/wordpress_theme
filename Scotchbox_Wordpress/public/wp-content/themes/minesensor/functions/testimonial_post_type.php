<?php
// Register Custom Post Type
function testimonial_post_type() {

	$labels = array(
		'name'                => _x( 'Testimonials', 'Post Type General Name', 'minesensor' ),
		'singular_name'       => _x( 'Testimonial', 'Post Type Singular Name', 'minesensor' ),
		'menu_name'           => __( 'Testimonials', 'minesensor' ),
		'name_admin_bar'      => __( 'Testimonials', 'minesensor' ),
		'parent_item_colon'   => __( 'Parent Item:', 'minesensor' ),
		'all_items'           => __( 'All Testimonials', 'minesensor' ),
		'add_new_item'        => __( 'Add New Testimonials', 'minesensor' ),
		'add_new'             => __( 'Add New', 'minesensor' ),
		'new_item'            => __( 'New Testimonials', 'minesensor' ),
		'edit_item'           => __( 'Edit Testimonials', 'minesensor' ),
		'update_item'         => __( 'Update Testimonials', 'minesensor' ),
		'view_item'           => __( 'View Item', 'minesensor' ),
		'search_items'        => __( 'Search Item', 'minesensor' ),
		'not_found'           => __( 'Not found', 'minesensor' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'minesensor' ),
	);
	$args = array(
		'label'               => __( 'Testimonial', 'minesensor' ),
		'description'         => __( 'Testimonial Minesensor', 'minesensor' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-pressthis',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'testimonial', $args );

}
add_action( 'init', 'testimonial_post_type');