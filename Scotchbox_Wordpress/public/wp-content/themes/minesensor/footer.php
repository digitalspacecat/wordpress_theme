<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <p class="copyright">Copyright &copy; <?php echo date('Y') ?> <?php bloginfo('name'); ?></p>
            </div>
            <div class="col-md-8">
                <?php
                        $args = array(
                            'theme_location' => 'top-menu',
                            'menu' => '',
                            'container' => '',
                            'menu_class' => 'menu',
                        );
                        wp_nav_menu( $args ); ?>
            </div>
        </div>
    </div>
</footer>
<!--.site-footer-->
<?php wp_footer(); ?>
</body>
</html>