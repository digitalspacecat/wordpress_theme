<?php
/*
	Template Name: Partner Page
*/
?>

<?php get_header() ?>
<?php while( have_posts() ) : the_post() ?>
<div class="partners">
<div class="head-banner" <?php echo header_banner_style(); ?>>
	<div class="overlay">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="banner-content">
						<h2>Partners</h2>
						<div class="diver"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><!--.head-banner-->

<div class="content">
	<div class="offices">
		<div class="container">
			<h3><?php the_title() ?></h3>
			<div class="diver"></div>

		<?php
			$args = array( 'post_type' => 'partner' );
			$partners = new WP_Query( $args ); $i = 1;
			if ( $partners->have_posts() ) : while ( $partners->have_posts() ) : $partners->the_post();
		?>
			<div class="partner">
				<div class="row">
					<div class="col-md-3 col-md-offset-1 col-xs-4">
						<div class="img">
							<?php if ( has_post_thumbnail() ) :
								the_post_thumbnail('full');
							endif; ?>
						</div>
					</div>

					<div class="col-md-7 col-sm-8">
						<div class="partner-info">
							<h4><?php the_title() ?></h4>
							<div><?php the_content() ?></div>
						</div>
					</div>
				</div>
			</div>
		<?php endwhile; wp_reset_query(); endif;?>
		</div>
	</div><!--.offices-->
</div><!--.content-->
</div>
<?php endwhile; ?>
<?php get_footer() ?>